'''aws-adfs-keyring

Returns json credentials used by credential_process in ~/.aws/config

Usage: aws-adfs-keyring [options] [--] STS_HOST STS_USER [AWS_ACCT [AWS_ROLE]]

With just the host and user will list all available roles. Providing only an 
account will list only the roles for that account. An Account is either an
An account ID or Alias. To do so we first attempt to authenticate with ADFS 
host. If the password is not stored in the keyring it will ask for the 
password on the command line and save it to the keyring if authentication is 
successful. If an account and role are provided it will see if the token is 
saved in the keyring. If not or the token is expired we authenticate with the 
host and see if they are listed in the provided selection and get the token 
for that role and save it to the keyring.

Options:
    -c CONFIG   Config file for saving profiles. [default: ~/.aws/config]
    -p PROFILE  Add this as a profile to aws config file.
    -r REGION   The region to set when saving or generating profiles.
    -U STS_URN  The URN of AWS in STS. [default: urn:amazon:webservices]
    -D SECONDS  Token duration in seconds. [default: 3600]
    -A PROFILE  Profile to use when acquiring credentials. [default: no_auth]
    --no-auth   Return empty creds.
    --generate  Add generated profiles for all roles to aws config file.
    --reset-pw  Delete the password saved in the keyring.
    -h --help   Show this.
    --version   Show version.

'''
from __future__ import print_function

import os
import sys
import json
import boto3
import docopt
import shutil
import requests
import getpass
import configparser as ConfigParser
import base64
import logging
import keyring
import xmltodict
from bs4 import BeautifulSoup
from dateutil.parser import parse as dtparse
from .keyringcookiejar import KeyringCookieJar

logging.basicConfig(stream=sys.stderr,
    level=logging.DEBUG if os.getenv('DEBUG') else logging.ERROR)

import pkg_resources
__version__ = pkg_resources.get_distribution('aws-adfs-keyring').version


def get_main_cli():
    cli = os.path.basename(sys.argv[0])
    if shutil.which(cli)==sys.argv[0]:
        return cli
    return sys.argv[0]


class AWSToken(dict):

    def __init__(self, token):
        if isinstance(token, str):
            token = json.loads(token)
        if 'Credentials' in token:
            token = token['Credentials']
        if not isinstance(token['Expiration'], str):
            token['Expiration'] = token['Expiration'].isoformat()
        if 'Version' not in token:
            token['Version'] = 1
        super(AWSToken, self).__init__(token)

    def __str__(self):
        return json.dumps(self)

    @property
    def expired(self):
        dt = dtparse(self['Expiration'])
        return dt <= dt.now(tz=dt.tzinfo)


class SAMLAssertion(dict):

    def __init__(self, assertion):
        super(SAMLAssertion, self).__init__(self.parse(assertion))
        self.assertion = assertion

    def parse(self, assertion):
        return xmltodict.parse(base64.b64decode(assertion))["samlp:Response"]
    
    def __str__(self):
        return self.assertion

    def __repr__(self):
        return json.dumps(self,indent=4)

    @property
    def attributes(self):
        attrs = self["Assertion"]["AttributeStatement"]["Attribute"]
        return {i["@Name"]:i["AttributeValue"] for i in attrs}

    @property
    def service(self):
        return self["Issuer"]["#text"]

    @property
    def login(self):
        return self["Assertion"]["Subject"]["NameID"]["#text"]

    @property
    def principal_map(self):
        roles = self.attributes["https://aws.amazon.com/SAML/Attributes/Role"]
        p_map = (role.split(',',1) for role in roles)
        return dict((s,f) if 'saml-provider' in f else (f,s) for f,s in p_map)


class AWS_ADFS(requests.Session):
    """AWS ADFS Authentication Handler"""

    def __init__(self, host, urn, username, reset_pw=False, auth_profile=None):
        super(AWS_ADFS, self).__init__()
        self.host = host
        self.urn = urn
        self.username = username
        self.reset_pw = reset_pw
        self.auth_profile = auth_profile
        if sys.platform != 'win32':
            self.cookies = KeyringCookieJar(svc=self.idp_url, acct=self.username)
        self._assertion = None
        self._role_map = None

    @property
    def password(self):
        if self.reset_pw:
            try:
                keyring.delete_password(self.idp_url, self.username)
            except keyring.errors.PasswordDeleteError as e:
                pass
            self.cookies.clear()
            self.reset_pw = False
        password = keyring.get_password(self.idp_url,self.username)
        if password is None:
            if sys.stdin.isatty() and sys.stderr.isatty():
                password = getpass.getpass()
        return password

    @property
    def idp_url(self):
        return 'https://{0.host}/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp={0.urn}'.format(self)

    @property
    def assertion(self):
        if self._assertion:
            return self._assertion
        if self.cookies and not self.reset_pw and sys.platform != 'win32':
            logging.debug('Getting cookie')
            auth_res = self.get(self.idp_url)
            if not auth_res.ok:
                self.cookies.clear()
            else:
                soup = BeautifulSoup(auth_res.text, "lxml")
                assertion = soup.find('input',{"name":'SAMLResponse'})['value']
                return SAMLAssertion(assertion)

        logging.debug('Getting assertion')

        form_res = self.get(self.idp_url)
        soup = BeautifulSoup(form_res.text,"lxml")

        form = soup.find('form',id='loginForm')

        frag_url = requests.compat.urlparse(form_res.url)
        frag_act = requests.compat.urlparse(form['action'])
        url = frag_act._replace(scheme=frag_url.scheme,netloc=frag_url.netloc).geturl()

        password = self.password

        data = {i['name']:i.get('value','') for i in form.find_all('input')}
        data[next(k for k in data if 'user' in k.lower())] = self.username
        data[next(k for k in data if 'pass' in k.lower())] = password

        auth_res = self.post(url, data=data)
        if auth_res.ok:
            keyring.set_password(self.idp_url, self.username, password)
            if sys.platform != 'win32':
                self.cookies.save(ignore_discard=True)
        soup = BeautifulSoup(auth_res.text, "lxml")
        assertion = soup.find('input',{"name":'SAMLResponse'})['value']

        return SAMLAssertion(assertion)

    def role_map(self, assertion=None):
        accts = self._role_map
        if accts:
            return accts

        r = self.post('https://signin.aws.amazon.com:443/saml', data={'SAMLResponse':str(assertion or self.assertion)})
        soup = BeautifulSoup(r.text,"lxml")

        accts = [a for a in soup.find_all('div','saml-account') if a.find('div','saml-account-name')]
        accts = {a.find('div','saml-account-name').text.split()[1]:{i.text:i['for']
            for i in a.find_all('label','saml-role-description')} for a in accts}
        self._role_map = accts
        return accts

    def get_token(self, account, role, duration=3600):
        service_name = 'token+{0.idp_url}'.format(self)
        username = '{0}/{1}'.format(account, role)

        token = keyring.get_password(service_name, username)
        if token:
            token = AWSToken(token)
            if token.expired:
                # keyring.delete_password(service_name, username)
                token = None
            else:
                return token

        logging.debug('Getting token')

        assertion = self.assertion
        role_arn = self.role_map(assertion)[account][role]
        sess = boto3.session.Session(profile_name=self.auth_profile) if self.auth_profile else boto3
        token = AWSToken(sess.client('sts').assume_role_with_saml(
            RoleArn=role_arn,
            PrincipalArn=assertion.principal_map[role_arn],
            SAMLAssertion=str(assertion),
            DurationSeconds=int(duration)))
        if token:
            keyring.set_password(service_name, username, str(token))

        return token

    def get_cli(self, account, role):
        opts = [get_main_cli()]

        if self.urn != 'urn:amazon:webservices':
            opts.append('-U')
            opts.append(self.urn)

        if self.auth_profile != 'no_auth':
            opts.append('-P')
            opts.append(self.auth_profile)

        cli = ' '.join(opts)

        fmt = '{0} {1.host} "{1.username}" {2} {3}'

        return fmt.format(cli, self, account, role)


def save_profiles(filename, auth_profile=None, **profiles):
    if not profiles:
        return

    config = ConfigParser.RawConfigParser()
    config.read(filename)

    if auth_profile:
        section = 'profile '+auth_profile
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, 'credential_process', '{0} --no-auth no auth'.format(get_main_cli()))

    for name, profile in profiles.items():
        section = name if name=='default' else 'profile '+name
        if not config.has_section(section):
            config.add_section(section)

        for k,v in profile.items():
            config.set(section, k, v)

    with open(filename, 'w+') as configfile:
        config.write(configfile)


def run(opts,outstream=sys.stdout):
    host, urn, username = opts['STS_HOST'], opts['-U'], opts['STS_USER']
    account, role = opts['AWS_ACCT'], opts['AWS_ROLE']
    generate, profile, region = opts['--generate'], opts['-p'], opts['-r']
    profiles = {}

    if opts['--no-auth']:
        print('{"Version":1,"AccessKeyId":"","SecretAccessKey":""}',file=outstream)
        return

    aws_adfs = AWS_ADFS(host, urn, username, opts['--reset-pw'], opts['-A'])

    if account and role:
        token = aws_adfs.get_token(account, role, opts['-D'])
        if not token:
            return
        if profile or generate:
            if not profile:
                profile = '{0}/{1}'.format(account, role)
            profiles[profile] = {'credential_process':aws_adfs.get_cli(account, role)}
            if region:
                profiles[profile]['region'] = region
        print(token,file=outstream)
    else:
        acct_roles = aws_adfs.role_map()
        for acct,roles in acct_roles.items():
            if account in (acct,False,None,''):
                print(acct+':',file=outstream)
                for r in roles:
                    if generate:
                        profile = '{0}/{1}'.format(acct, r)
                        profiles[profile] = {'credential_process':aws_adfs.get_cli(acct, r)}
                        if region:
                            profiles[profile]['region'] = region
                    print('  - '+r,file=outstream)

        save_profiles(os.path.expanduser(opts['-c']), opts['-A'], **profiles)


def main(doc=__doc__,version=__version__):
    run(docopt.docopt(doc,version=version))
