# AWS ADFS Keyring Authenticator

A simple way to automatically authenticate with ADFS to AWS and cache the credentials in your keyring.

## Install

Requires python 3.3 or above.

Install with pip:

```
pip install aws-adfs-keyring
```

## Setup

Just run:

```
aws-adfs-keyring --generate sts.example.com domain\\username
```

This will ask for your password and then attempt to authenticate. If successful it will generate a list of profiles named `account/ADFS-rolename`. It will also cache your password and sts cookie in your keyring.

Then you can use these profiles with aws-cli:

```
aws --profile=account/ADFS-rolename s3 ls
```

## Set default profile

Just run:

```
aws-adfs-keyring -p default sts.example.com domain\\username account role
```
